cmake_minimum_required(VERSION 3.0.2)
project(vision)


find_package(catkin REQUIRED COMPONENTS
  rospy
  message_generation
  cv_bridge
  geometry_msgs
  sensor_msgs
  std_msgs
)

catkin_python_setup()



###################################
## catkin specific configuration ##
###################################

catkin_package(
 CATKIN_DEPENDS
 cv_bridge
 geometry_msgs
 message_runtime
 rospy
 sensor_msgs
 std_msgs
 vision_driver
)

catkin_install_python(PROGRAMS
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

#############
## Install ##
#############

install(
  DIRECTORY
  DESTINATION "${CATKIN_PACKAGE_SHARE_DESTINATION}"
)
