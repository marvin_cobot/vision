#!/usr/bin/env python3

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    packages=['vision'],
    package_dir={'': '.'},
    requires=['rospy', 'std_msgs', 'sensors_msgs', 'cv_bridge', 'geometry_msgs']
)

setup(**setup_args)
