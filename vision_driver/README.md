# Vision Driver

## Overview

The **vision_driver** package is a ROS package designed for detecting platforms and objects in images, with a focus on locating AprilTag markers at specific platform corners. The package provides the tools to calculate cell positions on the platform and publishes relevant coordinates, allowing other nodes to retrieve and utilize platform layout information.

Key features:
- Detection of AprilTags markers in an image.
- Calculation of platform coordinates based on detected tags.
- Generation of a grid of cells for spatial positioning on the platform.
- Publishing of platform position data as ROS messages.

The **vision_driver** package is a component of the larger **vision** package, which is designed for image-based platform and object detection using ROS.

## Installation

### Prerequisites

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)
- [OpenCV](https://pypi.org/project/opencv-python/)
- [cv_bridge](http://wiki.ros.org/cv_bridge)
- [AprilTag](https://github.com/AprilRobotics)

### Install Dependencies

To install the required dependencies, use:

```bash
sudo apt-get install ros-noetic-cv-bridge
pip install opencv-python
pip install pupil-apriltags
```

### Building the package

Clone the **vision_driver** package into your ROS workspace and build it:
```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/marvin_cobot/vision.git
cd ~/catkin_ws
catkin build
source devel/setup.bash
```

## Configuration

The package includes configuration files that must be set up before running the node. All these files should be located within the **config** directory of the **vision_driver** package.

### Sensors Configuration File

The **sensors.yaml** file defines sensor settings and topics for acquiring RGB images used in detection.

Example structure of sensors.yaml:
```yaml
lidar:
  device_name: intel_realsense_d435
  rgb_image: /camera/color/image_raw
```
- **device_name**: The name of the device used for capturing images. In this example, it is an Intel RealSense D435 camera.
- **rgb_image**: The ROS topic providing the RGB image stream from the specified device. This topic is referenced by the node for processing the incoming image data.

### Tags Configuration File

The **tags.yaml** file contains the configuration for identifying specific AprilTags used as markers on the platform. This configuration is essential for locating the corners of the platform in an image by detecting unique AprilTags.

Example structure of tags.yaml:
```yaml
bottom_left:
  tag_id: 0
top_left:
  tag_id: 1
top_right:
  tag_id: 2
bottom_right:
  tag_id: 3
```
- **tag_id**: Each key represents a corner of the platform, and each tag is identified by a unique tag_id assigned to it. For example, bottom_left has a tag ID of 0, and top_right has a tag ID of 2.

The tags defined in this file must match the actual tags placed on the platform for correct detection. This setup allows the vision_driver node to accurately identify and position the platform based on detected tags.

### Platform Dimension Configuration File

The **platform_dimension.yaml** file specifies the dimensions of the platform in terms of the number of cells, both in width and height. This configuration allows the node to divide the detected platform area into a grid of cells accurately.

Example structure of platform_dimension.yaml:
```yaml
width: 20
height: 44
```
- **width**: The number of cells along the platform's width.
- **height**: The number of cells along the platform's height.

These values determine the grid layout used for dividing the platform into smaller sections (cells) and assigning coordinates to each cell in the grid.

### Topics Configuration File

The **topics.yaml** file defines the ROS topics used by nodes in the vision_driver package to publish data.

Example structure of topics.yaml:
```yaml
position: /vision/vision_driver/platform/position
```
- **position**: This is the topic on which the platform's position and cell data are published. The PlatformCroper node publishes messages containing the platform’s detected position and cell coordinates to this topic.

## Launch

```bash
roslaunch vision_driver driver.launch
```

## Licence

This project is licensed under the LGPL License

## Maintainers

- [Maxence Grand](Maxence.Grand@univ-grenoble-alpes.fr), Research Engineer, Laboratoire D'informatique de Grenoble, Univ. Grenoble Alpes, Grenoble 38000 France 
