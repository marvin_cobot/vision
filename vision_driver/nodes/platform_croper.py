#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Node: vision_driver_platform
============================

Description:
------------
This node detects the platform in a received image by identifying AprilTags markers at the platform's corners.
It computes the platform's position, divides it into cells, and publishes their coordinates.

Usage:
------
This node should be run with a compatible RGB image source as defined in `sensors.yaml`. When an image is received,
the node detects AprilTags to locate the platform's corners and computes cell positions if all tags are visible.
The result is published to `/vision/vision_driver/platform/position`.
"""

__author__ = "Maxence Grand"
__credits__ = ["Maxence Grand"]
__license__ = "LGPL"
__version__ = "1.0.0"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Released"

import rospy
import json, rospkg, yaml, sys
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from pupil_apriltags import Detector
from vision_driver.msg import PlatformMsg, Cell
import faulthandler
from threading import Lock

# YAML configuration file paths
SENSORS_YAML_FILE = "config/sensors.yaml"
PLATFORM_DIMENSION_YAML_FILE = "config/platform_dimension.yaml"
TAGS_YAML_FILE = "config/tags.yaml"
TOPICS_YAML_FILE = "config/topics.yaml"


def centroid(points):
    """Calculate the centroid of a given set of points.

    @param points: List of (x, y) tuples representing the corners of an AprilTag.
    @type points: List

    @return: Coordinates (x, y) of the computed centroid.
    @rtype: Tuple
    """
    x = [p[0] for p in points]
    y = [p[1] for p in points]
    centroid = (sum(x) / len(points), sum(y) / len(points))
    return centroid


def compute_cells(cv2_image, tags, dimension):
    """Compute cell coordinates on the platform based on detected AprilTags.

    Divides the platform into a grid of cells using the corners defined by the AprilTags
    at each corner of the platform.

    @param cv2_image: The received image in OpenCV format.
    @type cv2_image: numpy.ndarray
    @param tags: Platform message with coordinates of detected tags.
    @type tags: PlatformMsg

    @return: List of Cell messages with coordinates and cell indices.
    @rtype: list
    """
    w_top = int(tags.top_right.x - tags.top_left.x)
    w_bottom = int(tags.bottom_right.x - tags.bottom_left.x)
    h_left = int(tags.bottom_left.y - tags.top_left.y)
    h_right = int(tags.bottom_right.y - tags.top_right.y)
    (x0, y0) = (int(tags.top_left.x), int(tags.top_left.y))
    (x0_bottom, y0_bottom) = (int(tags.bottom_left.x), int(tags.bottom_left.y))

    cells = []

    for i in range(0, dimension['width']):
        for j in range(0, dimension['height']):
            height_cell = h_left / dimension['height']
            width_cell = (w_top / dimension['width']) if j < dimension['height'] / 2 else (w_bottom / dimension['width'])

            y1 = y0 + int(j * height_cell)
            y2 = y0 + int((j + 1) * height_cell)

            x1 = (x0 if j < dimension['height'] / 2 else x0_bottom) + int(i * width_cell)
            x2 = (x0 if j < dimension['height'] / 2 else x0_bottom) + int((i + 1) * width_cell)

            c = Cell()
            c.color = "undefined"
            c.i = i
            c.j = j
            c.top_left.x = x1
            c.top_left.y = y1
            c.top_right.x = x2
            c.top_right.y = y1
            c.bottom_right.x = x2
            c.bottom_right.y = y2
            c.bottom_left.x = x1
            c.bottom_left.y = y2

            cells.append(c)
    return cells


class PlatformCroper:
    """PlatformCroper class for detecting platform position and dividing it into cells based on AprilTags.

    This class subscribes to an RGB image topic, processes each image to detect AprilTags at the
    platform's corners, and publishes the platform's position and cell grid coordinates when all
    tags are detected.
    """

    def __init__(self):
        """Initializes the PlatformCroper.

        Sets up the necessary publishers, loads configuration files for topic and tag information,
        and initializes the AprilTag detector.
        """
        self.load_yaml()
        self.bridge = CvBridge()
        self.detector = Detector(families="tag36h11")
        self.platform_msg = None
        self.has_frame = False

        self.pub_platform_position = rospy.Publisher(self.topics['position'], PlatformMsg, queue_size=1)
        rospy.loginfo(f"{self.topics['position']} ready")

        rospy.loginfo(f"vision_driver_platform subscribes to {self.sensors['lidar']['rgb_image']}")
        rospy.Subscriber(
            self.sensors['lidar']['rgb_image'],
            Image,
            self.callback)

    def load_yaml(self):
        """Load YAML configurations for sensors, tags, platform dimensions, and topics.

        Loads the required configuration data for sensor and topic settings, AprilTags, and platform dimensions.
        """
        rospack = rospkg.RosPack()
        path = rospack.get_path("vision_driver")

        yaml_file = f"{path}/{SENSORS_YAML_FILE}"
        self.sensors = yaml.safe_load(open(yaml_file))

        yaml_file = f"{path}/{TAGS_YAML_FILE}"
        self.tags = yaml.safe_load(open(yaml_file))

        yaml_file = f"{path}/{PLATFORM_DIMENSION_YAML_FILE}"
        self.dimension = yaml.safe_load(open(yaml_file))

        yaml_file = f"{path}/{TOPICS_YAML_FILE}"
        self.topics = yaml.safe_load(open(yaml_file))

    def publish(self, msg):
        """Publish the platform's position message.

        @param msg: The platform position and cell data to be published.
        @type msg: PlatformMsg
        """
        self.pub_platform_position.publish(msg)

    def callback(self, image):
        """Process an incoming image, detect platform tags, and compute cell positions.

        Detects AprilTag markers at the platform's corners in the received image. If all
        required tags are detected, it calculates the cell positions on the platform and
        publishes the platform's position and cell layout.

        @param image: The received ROS Image message.
        @type image: sensor_msgs/Image
        """
        self.has_frame = False
        image = self.bridge.imgmsg_to_cv2(image)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        tags = self.detector.detect(gray)

        platform_msg = PlatformMsg()
        b1 = b2 = b3 = b4 = False

        for t in tags:
            (cx, cy) = centroid(t.corners)
            if t.tag_id == self.tags['top_left']['tag_id']:
                platform_msg.top_left.x = cx
                platform_msg.top_left.y = cy
                b1 = True
            elif t.tag_id == self.tags['top_right']['tag_id']:
                platform_msg.top_right.x = cx
                platform_msg.top_right.y = cy
                b2 = True
            elif t.tag_id == self.tags['bottom_left']['tag_id']:
                platform_msg.bottom_left.x = cx
                platform_msg.bottom_left.y = cy
                b3 = True
            elif t.tag_id == self.tags['bottom_right']['tag_id']:
                platform_msg.bottom_right.x = cx
                platform_msg.bottom_right.y = cy
                b4 = True

        if b1 and b2 and b3 and b4:
            self.has_frame = True
            platform_msg.cells = compute_cells(image, platform_msg, self.dimension)
            self.publish(platform_msg)


def main():
    """Main function to initialize the node and run the PlatformCroper.

    This function initializes the ROS node, enables fault handling, and creates an instance
    of the PlatformCroper class. It keeps the node active using rospy.spin().
    """
    faulthandler.enable()
    rospy.init_node("vision_driver_platform", anonymous=True)
    platform = PlatformCroper()
    rospy.loginfo("Platform Node Initialized")
    rospy.spin()


if __name__ == "__main__":
    main()
