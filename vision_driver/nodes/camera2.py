#! /usr/bin/env python3.8
# -*- coding: utf-8 -*-
"""
| author:
| Belal HMEDAN,
| LIG lab/ Marvin Team,
| France, 2022.
| image processing node publisher.
"""

import rospy
from std_msgs.msg import String, Bool
import json
import time
import numpy as np
import pyrealsense2.pyrealsense2 as rs

class VisionDriver:

    def __init__(self):
        """
        Class VisionDriver: Driver to do scene segmentation using
          Realsense L515 camera and ROS.
        ---
        Parameters:
        @param: None.
        """
        self.rgbP_   = RGBProcessor()
        self.depthP_ = DepthProcessor()

        self.frame_idx = 0
        self.latch = np.ones((11, 23, 3), np.uint8)

        self.init_cam()

    def init_cam(self):
        """
        Function: init_cam, to set the Realsense L515 camera parameters.
        ---
        Parameters:
        @param: None
        ---
        @return None.
        """
        # Start reading frames
        self.pipeline = rs.pipeline()
        self.config = rs.config()
        # RGB Stream
        self.config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 30)
        self.config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        #
        self.profile = self.config.resolve(self.pipeline)

        # Start streaming
        self.pipeline.start(self.config)

        # Declare sensor object and set options
        depth_sensor = self.profile.get_device().first_depth_sensor()
        depth_sensor.set_option(rs.option.visual_preset, 5) # 5 is short range, 3 is low ambient light
        depth_sensor.set_option(rs.option.receiver_gain, 8)
        depth_sensor.set_option(rs.option.pre_processing_sharpening, 0.0)
        depth_sensor.set_option(rs.option.post_processing_sharpening, 3.0)
        depth_sensor.set_option(rs.option.laser_power, 100)
        depth_sensor.set_option(rs.option.confidence_threshold, 1)
        depth_sensor.set_option(rs.option.digital_gain, 2.0)
        depth_sensor.set_option(rs.option.noise_filtering, 4)
        # Get the sensor once at the beginning. (Sensor index: 1)
        sensor = self.pipeline.get_active_profile().get_device().query_sensors()[1]

        # Set the exposure anytime during the operation
        sensor.set_option(rs.option.exposure, 800.000)
        sensor.set_option(rs.option.gain, 0.000)

        # # Filters
        self.threshold_filter = rs.threshold_filter(min_dist=0.8, max_dist=1.25)
        self.temporal_filter  = rs.temporal_filter(smooth_alpha=0.1, smooth_delta = 90.0, persistence_control=0)

        # # Colorizer
        self.colorizer = rs.colorizer()
        self.colorizer.set_option(rs.option.visual_preset, 1) # 0=Dynamic, 1=Fixed, 2=Near, 3=Far
        self.colorizer.set_option(rs.option.min_distance, 0.8)
        self.colorizer.set_option(rs.option.max_distance, 1.25)

        # # Align element
        align_to = rs.stream.color
        self.align = rs.align(align_to)

    def init_ros(self):
        """
        Function: init_ros, to set the ROS node/publishers/subscriber.
        ---
        Parameters:
        @param: None
        ---
        @return None.
        """
        self.dict_pub = rospy.Publisher("vision_dict", String, queue_size=10)
        self.hand_pub = rospy.Publisher("hand_detection", Bool, queue_size=10)
        rospy.init_node("perception", anonymous=True)
        self.rate = rospy.Rate(10)  # 10hz

    def run(self, colorizer=False, lock_delay=1):
        """
        Function: run, to run the vision system.
        ---
        Parameters:
        @param: colorizer, bool, to colorize the depth.
        @param: lock_delay, int, time delay for hand or arm locks.

        ---
        @return None.
        """
        skipper = 0

        while not rospy.is_shutdown():
            skipper += 1
            frames = self.pipeline.wait_for_frames()
            if skipper < 5:
                continue
            else:
                skipper -= 1

            # Align Frames
            frames = self.align.process(frames)

            color_frame = frames.get_color_frame()
            depth_frame = frames.get_depth_frame()
            if (not depth_frame) or (not color_frame):
                continue

            # Depth
            depth_frame = self.threshold_filter.process(depth_frame)
            depth_frame = self.temporal_filter.process(depth_frame)

            # Convert images to numpy arrays
            color_array = np.asanyarray(color_frame.get_data())
            if colorizer:
                depth_array = np.asanyarray(colorizer.colorize(depth_frame).get_data())
            else:
                depth_array = np.asanyarray(depth_frame.get_data())

            ################
            ## Processing ##
            ################
            self.rgbP_.get_rgb_info(color_array)

            self.hand_pub.publish(self.rgbP_.hand)

            # Hand Detected!
            if self.rgbP_.hand:
                rospy.logwarn("\nHand Detected, sleeping {} Seconds ...\n".format(lock_delay))
                time.sleep(lock_delay)
                continue

            valid_depth = self.depthP_.get_depth_info(depth_array, self.rgbP_.greenRect)
            if(not valid_depth):
                rospy.logwarn("Invalid Depth Frame")
                continue

            corners = ["p_01_07", "p_01_15", "p_07_07", "p_07_15"]
            self.frame_idx += 1

            for k in self.vision_dict:
                # Color
                self.vision_dict[k][0] = self.rgbP_.cellsState[k]

                x, y = self.depthP_.key2pos(k)

                self.latch[x, y, self.frame_idx-1] = int(self.depthP_.cellsState[k])

                ## Correct Depth from Color
                if(self.vision_dict[k][0] == 'g'):
                    self.latch[x, y] = (0, 0, 0)

                if(self.vision_dict[k][1] == 0 and self.vision_dict[k][0] != 'g'):
                    self.latch[x, y] = (1, 1, 1)

                ## Green Corners
                if (k in corners):
                    self.latch[x, y] = (1, 1, 1)

                ## Latch Memory
                if (self.latch[x, y, 0] == self.latch[x, y, 1]) and (self.latch[x, y, 0] == self.latch[x, y, 2]):
                    self.vision_dict[k][1] = int(self.latch[x, y, 0])

            my_dict = json.dumps(self.vision_dict)

            ## Reset Sequence Counter
            if(self.frame_idx % 3 == 0):
                self.frame_idx = 0

            ## Publish vision Data
            self.dict_pub.publish(my_dict)
            self.rate.sleep()

if __name__ == "__main__":
    try:
        vd_ = VisionDriver()
        vd_.init_ros()
        vd_.run()

    except rospy.ROSInterruptException:
        pass
