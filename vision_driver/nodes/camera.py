#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A Camera Node."""

__author__ = "Belal Hmedan"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import pyrealsense2 as rs
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import rospy

rospy.init_node("camera", anonymous=True)
rospy.loginfo("camera Node Initialized")

publisher = rospy.Publisher(
	'/vision/vision_driver/image',
	Image,
	queue_size=1
)

rospy.loginfo('/vision/vision_driver/image ready')

bridge = CvBridge()

pipe = rs.pipeline()
cfg  = rs.config()

cfg.enable_stream(rs.stream.color, 1280,720, rs.format.bgr8, 30)

pipe.start(cfg)

r = rospy.Rate(30)
while not rospy.is_shutdown():
    frame = pipe.wait_for_frames()
    color_frame = frame.get_color_frame()

    bgr_image = np.asanyarray(color_frame.get_data())

    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)

    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)

    publisher.publish(bridge.cv2_to_imgmsg(rgb_image))

    cv2.imshow('rgb', bgr_image)

    if cv2.waitKey(1) == ord('q'):
        break

pipe.stop()