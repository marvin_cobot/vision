# Vision Interface

## Overview

The **vision_interface** package is a ROS package designed for detecting colors and human hands. The package provides the tools to calculate cell colors on the platform and hands positions on the platform and publishes relevant colors and coordinates, allowing other nodes to retrieve and utilize platform layout information.

Key features:
- Color detection and calibration.
- Hand detection.

The **vision_interface** package is a component of the larger **vision** package, which is designed for image-based platform and object detection using ROS.

## Installation

### Prerequisites

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)
- [OpenCV](https://pypi.org/project/opencv-python/)
- [cv_bridge](http://wiki.ros.org/cv_bridge)
- [vision_driver](https://gitlab.com/marvin_cobot/vision/vision_driver)

### Install Dependencies

To install the required dependencies, use:

```bash
sudo apt-get install ros-noetic-cv-bridge
pip install opencv-python
pip install pupil-apriltags
```

### Building the package

Clone the **vision_interface** package into your ROS workspace and build it:
```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/marvin_cobot/vision.git
cd ~/catkin_ws
catkin build
source devel/setup.bash
```

## Configuration

The package includes configuration files that must be set up before running the node. All these files should be located within the **config** directory of the **vision_interface** package.

### Sensors Configuration File

The **sensors.yaml** file defines sensor settings and topics for acquiring RGB images used in color and hand detection.

Example structure of sensors.yaml:
```yaml
rgb_image: /camera/color/image_raw
platform_position: /vision/vision_driver/platform/position
```
- **rgb_image**: The ROS topic providing the RGB image stream. This topic is referenced by the node for processing the incoming image data.
- **platform_position**: The ROS Topics providing the platform position..

### Platform Dimension Configuration File

The **platform_dimension.yaml** file specifies the dimensions of the platform in terms of the number of cells, both in width and height. This configuration allows the node to divide the detected platform area into a grid of cells accurately.

Example structure of platform_dimension.yaml:
```yaml
width: 20
height: 44
```
- **width**: The number of cells along the platform's width.
- **height**: The number of cells along the platform's height.

These values determine the grid layout used for dividing the platform into smaller sections (cells) and assigning coordinates to each cell in the grid.

### Topics Configuration File

The **topics.yaml** file defines the ROS topics used by nodes in the vision_interface package to publish data.

Example structure of topics.yaml:
```yaml
cell_state: /vision/vision_interface/cell_state
human_pose: /vision/vision_interface/human_pose
```
- **position**: This is the topic on which the platform's position and cell data are published. The PlatformCroper node publishes messages containing the platform’s detected position and cell coordinates to this topic.

## Launch

```bash
roslaunch vision_interface Interface.launch
```

## Licence

This project is licensed under the LGPL License

## Maintainers

- [Maxence Grand](Maxence.Grand@univ-grenoble-alpes.fr), Research Engineer, Laboratoire D'informatique de Grenoble, Univ. Grenoble Alpes, Grenoble 38000 France
