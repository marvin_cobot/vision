#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
The hand detection node, responsible for detecting hand presence within cells and publishing hand poses.

This node subscribes to cell state updates and publishes detected human hand positions within a table.
"""

__author__ = "Loid Mazou"
__credits__ = ["Loic Mazou, Maxence Grand"]
__license__ = "LGPL"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json, yaml
import os, rospkg
import numpy as np



import cv2
import math

from vision_interface.msg import CellState, Hand, HumanPose
from vision_driver.msg import Cell

def floor(x, min):
	"""Restrict value to a minimum limit.

    @param x: The value to check.
    @param min: Minimum threshold.
    @return: The greater of x and min.
    """
	if(x < min):
		return min
	else:
		return x

def get_bgr(rgb):
	"""Convert RGB to BGR format with added alpha.

    Reverses RGB color order and adds alpha for image display.

    @param rgb: List of RGB values.
    @return: List of BGR values with an alpha channel.
    """
	c = []
	i = len(rgb)-1
	while i >= 0:
		c.append(rgb[i])
		i-=1
	c.append(255)
	return c

class Hand_Detector2:
	"""Class for detecting hands based on cell state."""

	def __init__(self):
		"""Initialize Hand_Detector2 with required parameters and ROS setup.

        Loads table configuration, initializes the image frame, loads cell colors,
        subscribes to cell state topic, and sets up a publisher for human pose detection.
        """
		self.has_frame = False
		rospack = rospkg.RosPack()
		path = rospack.get_path("vision_interface")
		self.frame = cv2.imread(f"{path}/database/background.png")
		self.colors = json.load(open(f"{path}/database/colors.json"))
		self.table = yaml.safe_load(open(f"{path}/config/table.yaml"))
		self.pub_topics = yaml.safe_load(open(f"{path}/config/topics.yaml"))
		self.h_cells = self.table['height']
		self.w_cells = self.table['width']
		rospy.loginfo("cell_state_visualization subscribes to the /vision/vision_interface/cell_state topic")
		rospy.Subscriber(
			"/vision/vision_interface/cell_state",
			CellState,
			self.search_hand)
		self.pub_humanPose = rospy.Publisher(
			self.pub_topics['human_pose'],
			HumanPose,
			queue_size=1
		)

	def search_hand(self, state) :
		"""Detects hand presence in cells based on color and position.

        Creates a table representation with color data, searches for hand presence,
        updates hand pose, and publishes the result.

        @param state: CellState message containing current cell colors.
        """
		tab = np.empty((self.w_cells+2, self.h_cells+2), dtype="<U5")

		for y in range(self.h_cells+2):
			tab[0, y] = "green"
			tab[self.w_cells+1, y] = "green"

		for x in range(self.w_cells+2):
			tab[x, 0] = "green"
			tab[x, self.h_cells+1] = "green"

		for cell in state.cells :
			tab[cell.i+1, cell.j+1] = str(cell.color)

		has_hand = False
		XY = []
		humanPose = HumanPose()
		for x in range(3, self.w_cells-1) :
			color = tab[x, self.h_cells]
			if str(color) != "green" :
				has_hand = True
				x_dep = x

				hand = Hand()
				direction = 0
				x = x_dep
				y = self.h_cells
				init = False
				while(x != x_dep or y != self.h_cells or init == False) :
					init = True
					cell = Cell()
					cell.i = x
					cell.j = y
					XY+=[(x,y)]
					hand.occupied = hand.occupied + [cell]
					direction = (direction - 2)%4
					for i in range(4):
						direction = (direction + 1)%4
						if direction == 0 :
							if str(tab[x, y-1]) != "green" :
								# print(tab[x, y-1])
								y=y-1
								break
						if direction == 1 :
							if str(tab[x+1, y]) != "green" :
								x=x+1
								break
						if direction == 2 :
							if str(tab[x, y+1]) != "green" :
								y=y+1
								x2 = x-1
								if not (x,y) in XY :
									while not (x2,y) in XY and x2>=0 :
										XY+=[(x2,y)]
										cell = Cell()
										cell.i = x2
										cell.j = y
										hand.occupied = hand.occupied + [cell]
										x2 = x2 - 1
									# if x2<0 :
									# 	print(x,y)
								break
						if direction == 3 :
							if str(tab[x-1, y]) != "green" :
								x=x-1
								break
				# search line with hand
				lines = {}
				for c in hand.occupied:
					i = c.i
					j = c.j
					if(j not in lines.keys()):
						lines[j] = {'min':i, 'max':i}
					else:
						if(i < lines[j]['min']):
							lines[j]['min'] = i
						elif(i > lines[j]['max']):
							lines[j]['max'] = i
				jmin = self.h_cells
				jmin_x = {'max':0,'min':0}
				for j in lines.keys():
					if(j <= jmin):
						jmin=j
						jmin_x['min'] = lines[j]['min']
						jmin_x['max'] = lines[j]['max']
					for i in range(floor(lines[j]['min']-6,0), lines[j]['min']):
						cell = Cell()
						cell.i = i
						cell.j = j
						hand.possibly_occupied = hand.possibly_occupied + [cell]
					for i in range(lines[j]['max']+1, lines[j]['max']+7):
						cell = Cell()
						cell.i = i
						cell.j = j
						hand.possibly_occupied.append(cell)
				for j in range(floor(jmin-4,0), jmin):
					for i in range(floor(jmin_x['min']-5,0), jmin_x['max']+6):
						cell = Cell()
						cell.i = i
						cell.j = j
						hand.possibly_occupied.append(cell)
				humanPose.hands += [hand]
				for (x,y) in XY :
					tab[x,y] = "green"

			color = tab[x, 1]
			if str(color) != "green" :
				has_hand = True
				x_dep = x

				hand = Hand()
				direction = 0
				x = x_dep
				y = 1
				init = False
				while(x != x_dep or y != 1 or init == False) :
					init = True
					cell = Cell()
					cell.i = x
					cell.j = y
					XY+=[(x,y)]
					hand.occupied = hand.occupied + [cell]
					direction = (direction - 2)%4
					for i in range(4):
						direction = (direction + 1)%4
						if direction == 0 :
							if str(tab[x, y-1]) != "green" :
								# print(tab[x, y-1])
								y=y-1
								break
						if direction == 1 :
							if str(tab[x+1, y]) != "green" :
								x=x+1
								break
						if direction == 2 :
							if str(tab[x, y+1]) != "green" :
								y=y+1
								x2 = x-1
								if not (x,y) in XY :
									while not (x2,y) in XY and x2>=0 :
										XY+=[(x2,y)]
										cell = Cell()
										cell.i = x2
										cell.j = y
										hand.occupied = hand.occupied + [cell]
										x2 = x2 - 1
									# if x2<0 :
									# 	print(x,y)
								break
						if direction == 3 :
							if str(tab[x-1, y]) != "green" :
								x=x-1
								break
				# search line with hand
				lines = {}
				for c in hand.occupied:
					i = c.i
					j = c.j
					if(j not in lines.keys()):
						lines[j] = {'min':i, 'max':i}
					else:
						if(i < lines[j]['min']):
							lines[j]['min'] = i
						elif(i > lines[j]['max']):
							lines[j]['max'] = i
				jmin = self.h_cells
				jmin_x = {'max':0,'min':0}
				for j in lines.keys():
					if(j <= jmin):
						jmin=j
						jmin_x['min'] = lines[j]['min']
						jmin_x['max'] = lines[j]['max']
					for i in range(floor(lines[j]['min']-6,0), lines[j]['min']):
						cell = Cell()
						cell.i = i
						cell.j = j
						hand.possibly_occupied = hand.possibly_occupied + [cell]
					for i in range(lines[j]['max']+1, lines[j]['max']+7):
						cell = Cell()
						cell.i = i
						cell.j = j
						hand.possibly_occupied.append(cell)
				for j in range(floor(jmin-4,0), jmin):
					for i in range(floor(jmin_x['min']-5,0), jmin_x['max']+6):
						cell = Cell()
						cell.i = i
						cell.j = j
						hand.possibly_occupied.append(cell)
				humanPose.hands += [hand]
				for (x,y) in XY :
					tab[x,y] = "green"

		# self.show_state(state, XY)
		self.pub_humanPose.publish(humanPose)


def main():
	rospy.init_node("hand_detector2", anonymous=True)
	hand_detector2 = Hand_Detector2()
	rospy.loginfo('Node hand_detector2 initialized')
	rospy.spin()

if __name__ == "__main__":
	main()
