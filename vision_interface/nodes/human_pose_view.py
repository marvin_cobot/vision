from __future__ import print_function

""" A visualization tool for cell states."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

WIDTH=1440
HEIGHT=720

import rospy
import json, yaml
import os, rospkg

from vision_interface.msg import HumanPose, Hand

import cv2
import math

global frame


def get_bgr(rgb):
	c = []
	i = len(rgb)-1
	while i >= 0:
		c.append(rgb[i])
		i-=1
	c.append(255)
	return c

class Viewer:
	"""The cell state viewer"""

	def __init__(self):
		"""Constructs the viewer"""
		self.has_frame = False
		rospack = rospkg.RosPack()
		self.path = rospack.get_path("vision_interface")

		self.frame = cv2.imread(f"{self.path}/database/background.png")
		self.colors = json.load(open(f"{self.path}/database/colors.json"))
		self.table = yaml.safe_load(open(f"{self.path}/config/table.yaml"))
		self.h_cells = self.table['height']
		self.w_cells = self.table['width']
		rospy.loginfo("cell_state_visualization subscribes to the /vision/vision_interface/cell_state topic")
		rospy.Subscriber(
			"/vision/vision_interface/human_pose",
			HumanPose,
			self.show_pose)

		for i in range(self.w_cells):
			for j in range(self.h_cells):
				start = (
					int((i/44) * WIDTH),
					int((j/20) * HEIGHT)
				)

				end = (
					int(((i + 1)/44) * WIDTH),
					int(((j + 1)/20) * HEIGHT)
				)

				self.frame = cv2.rectangle(self.frame, start, end, [255,255,255], -1)
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,255], 2)

	def show_pose(self, pose):
		"""Draw the cell state, i.e. draw all cells with their color"""
		self.has_frame = False

		frame = cv2.imread(f"{self.path}/database/background.png")
		#draw table
		for i in range(self.w_cells):
			for j in range(self.h_cells):
				start = (
					int((i/44) * WIDTH),
					int((j/20) * HEIGHT)
				)

				end = (
					int(((i + 1)/44) * WIDTH),
					int(((j + 1)/20) * HEIGHT)
				)

				frame = cv2.rectangle(frame, start, end, [255,255,255], -1)
				frame = cv2.rectangle(frame, start, end, [0,0,0,255], 2)

		for hand in pose.hands:

			for cell in hand.possibly_occupied:
				start = (
					int((cell.i/44) * WIDTH),
					int((cell.j/20) * HEIGHT)
				)

				end = (
					int(((cell.i + 1)/44) * WIDTH),
					int(((cell.j + 1)/20) * HEIGHT)
				)
				frame = cv2.rectangle(frame, start, end, [0,165,255,255], -1)
				frame = cv2.rectangle(frame, start, end, [0,0,0,255], 2)
			for cell in hand.occupied:
				start = (
					int((cell.i/44) * WIDTH),
					int((cell.j/20) * HEIGHT)
				)

				end = (
					int(((cell.i + 1)/44) * WIDTH),
					int(((cell.j + 1)/20) * HEIGHT)
				)
				frame = cv2.rectangle(frame, start, end, [0,0,255,255], -1)
				frame = cv2.rectangle(frame, start, end, [0,0,0,255], 2)
		self.frame = frame

def main():
	rospy.init_node("human_pose_visualization", anonymous=True)
	viewer = Viewer()
	rospy.loginfo('Node cell_state_visualization initialized')

	r = rospy.Rate(30)
	while not rospy.is_shutdown():
		cv2.imshow("Hands Viewer",viewer.frame)
		r.sleep()
		if(cv2.waitKey(33) & 0xFF in (ord('q'),27)):
			cv2.destroyAllWindows()
			break

if __name__ == "__main__":
	main()
