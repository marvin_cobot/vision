#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A Color Calibrator Node.

This node is designed to calibrate colors for cells in an image. It subscribes to image
and platform position topics, calculates dominant colors in specific regions of an image,
and allows manual color calibration by the user. The calibrated color values are saved
for future use in JSON and CSV formats.
"""


__author__ = "Maxence Grand"
__credits__ = ["Maxence Grand, Loic Mazou"]
__license__ = "LGPL"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"


import rospy

import json, rospkg, yaml, sys

import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from statistics import mean

from sensor_msgs.msg import Image
from vision_driver.msg import PlatformMsg
import csv


WIDTH=1440
HEIGHT=720

def centroid(points):
    """Compute the centroid of a set of points.

    @param points: List of 2D points to compute the centroid for.
    @type points: list of tuples

    @return: Coordinates of the centroid.
    @rtype: tuple
    """
    x = [p[0] for p in points]
    y = [p[1] for p in points]
    centroid = (sum(x) / len(points), sum(y) / len(points))
    return centroid

def get_dominant_color(inp_img):
    """
    Function: get_dominant_color, to get the dominant color pixel values.
    https://stackoverflow.com/a/61730849

    param inp_img: Input image to analyze.
    @type inp_img: ndarray

    @return: Dominant color in HSV format.
    @rtype: list
    """
    img = inp_img.copy()
    out = cv2.resize(img, (1, 1), 0)
    return out[0, 0]



class Reader:
    """Reader class to process color calibration on the received image."""

    def __init__(self):
        """Initialize the Reader.

        Sets up ROS subscriptions, loads configuration files, and initializes color values.
        """
        self.has_frame = False
        self.has_platform_position = False
        rospack = rospkg.RosPack()
        path = rospack.get_path("vision_interface")
        yaml_file = f"{path}/config/sensors.yaml"
        self.topics = yaml.safe_load(open(yaml_file))
        self.frame = cv2.imread(f"{path}/database/background.png")
        rospy.loginfo(f"color_calibration subscribes to the {self.topics['rgb_image']} topic")
        rospy.Subscriber(
            self.topics['rgb_image'],
            Image,
            self.callback_rgb
        )
        self.table = yaml.safe_load(open(f"{path}/config/table.yaml"))
        self.h_cells = self.table['height']
        self.w_cells = self.table['width']
        rospy.loginfo(f"color_calibration subscribes to the {self.topics['platform_position']} topic")
        rospy.Subscriber(
            self.topics['platform_position'],
            PlatformMsg,
            self.callback_platform)

        self.color_values = {
            'shadow':[],
            'green':[],
            'black':[],
            'white':[],
            'orange':[],
            'olive':[],
            'pink':[],
            'red':[],
            'purple1':[],
            'purple2':[],
            'grey1':[],
            'grey2':[],
            'yellow1':[],
            'yellow2':[],
            'brown1':[],
            'brown2':[],
            'brown3':[],
            'brown4':[],
            'blue1':[],
            'blue2':[],
            'blue3':[],
            'blue4':[]
        }

    def compute_cells(self, cv2_image, platform):
        """Compute cell coordinates for each cell on the platform.

        Uses the platform corner coordinates to calculate the position of each cell.

        @param cv2_image: Received image from which cells are calculated.
        @type cv2_image: ndarray
        @param platform: Platform dimensions with corner coordinates.
        @type platform: PlatformMsg

        @return: Dictionary with cell coordinates.
        @rtype: dict
        """

        #Compute widths and heights of the platform
        w_top = int(platform.top_right.x - platform.top_left.x)
        w_bottom = int(platform.bottom_right.x - platform.bottom_left.x)
        h_left = int (platform.bottom_left.y - platform.top_left.y)
        h_right = int (platform.bottom_right.y - platform.top_right.y)

        #Compute platform of the platform
        (x0,y0) = (int(platform.top_left.x),int(platform.top_left.y))
        (x0_bottom,y0_bottom) = (
            int(platform.bottom_left.x),
            int(platform.bottom_left.y)
        )

        #Compute coordinates of each cell
        cells = {}
        for i in range(self.w_cells):
            cells[i] = {}
            for j in range(self.h_cells):
                height_cell = h_left/self.h_cells
                width_cell = (w_top/self.w_cells) if j < self.h_cells/2 else (w_bottom/self.w_cells)

                y1 = y0 + int(j*height_cell)
                y2 = y0 + int((j+1)*height_cell)

                x1 = (x0 if j < self.h_cells/2 else x0_bottom) + int(i*width_cell)
                x2 = (x0 if j < self.h_cells/2 else x0_bottom) + int((i+1)*width_cell)

                A = (x1,y1)
                B = (x2,y1)
                C = (x2,y2)
                D = (x1,y2)
                cells[i][j] = [A,B,C,D]
        return cells
    def callback_platform(self, plat_pos):
        """Update the platform position.

        @param plat_pos: The platform position message.
        @type plat_pos: PlatformMsg
        """
        self.platform_position = plat_pos
        self.has_platform_position = True

    def callback_rgb(self, image):
        """Process the received RGB image for color calibration.

        Computes the cells' coordinates and updates the display with calibrated colors.

        @param image: The received image.
        @type image: Image
        """
        if(self.has_platform_position):
            bridge = CvBridge()
            cv2_image = cv2.cvtColor(
                bridge.imgmsg_to_cv2(image),
                cv2.COLOR_BGR2RGB
            )
            self.has_frame = True
            cells_pixels= self.compute_cells(cv2_image, self.platform_position)

            for i in range(self.w_cells):
                for j in range(self.h_cells):
                    [A,_,C,_] = cells_pixels[i][j]

                    tmp = cv2_image[A[1]:C[1]]

                    img_cell = np.array([tmp[i][A[0]:C[0]] for i in range(tmp.shape[0])] )
                    bgr = list(get_dominant_color(img_cell))
                    bgr_ = [int(bgr[0]),int(bgr[1]),int(bgr[2]),255]
                    start = (
                        int(i*(WIDTH/self.w_cells)),
                        int(j*(HEIGHT/self.h_cells))
                    )
                    end = (
                        int((i+1)*(WIDTH/self.w_cells)),
                        int((j+1)*(HEIGHT/self.h_cells))
                    )
                    self.frame = cv2.rectangle(self.frame, start, end, bgr_, -1)
                    self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,255], 2)

    def click_hsv(self, event, x, y, flags, param):
        """Handle mouse click events for HSV color selection.

        Adds the selected color values in HSV and RGB to the color values dictionary.

        @param event: The event type.
        @param x: x-coordinate of the click.
        @param y: y-coordinate of the click.
        """
        hsv_frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
        rgb_frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)

        if event == cv2.EVENT_LBUTTONDOWN:
            # print(f"{(x,y)}")
            rgb = rgb_frame[y][x]
            hsv = hsv_frame[y][x]
            self.color_values[self.current_color].append({'hsv':hsv, 'rgb':rgb})
            print(f'add values hsv:{hsv} rgb:{rgb}')

    def export_values(self):
        """Export RGB and HSV values into a csv and a json file"""
        self.export_csv_values()
        self.export_json_values()

    def export_csv_values(self):
        """Export RGB and HSV values into a csv file"""
        rospack = rospkg.RosPack()
        path = rospack.get_path("vision_interface")
        csvfile = f"{path}/database/color_values.csv"
        with open(csvfile, 'w', newline='') as csvfile:
            wrt = csv.writer(csvfile, delimiter=',', quotechar='|')
            wrt.writerow(['color_name', 'r', 'g', 'b', 'h', 's', 'v'])
            for c in self.color_values.keys():
                for sample in self.color_values[c]:
                    wrt.writerow([
                        c,
                        sample['rgb'][0],
                        sample['rgb'][1],
                        sample['rgb'][2],
                        sample['hsv'][0],
                        sample['hsv'][1],
                        sample['hsv'][2],
                    ])

    def export_json_values(self):
        """Export RGB and HSV values into a json file"""
        colors = {}
        for c in self.color_values.keys():
            r_values = []
            g_values = []
            b_values = []
            h_values = []
            s_values = []
            v_values = []
            for sample in self.color_values[c]:
                r_values.append(sample['rgb'][0])
                g_values.append(sample['rgb'][1])
                b_values.append(sample['rgb'][2])
                h_values.append(sample['hsv'][0])
                s_values.append(sample['hsv'][1])
                v_values.append(sample['hsv'][2])
            r = int(mean(r_values))
            g = int(mean(g_values))
            b = int(mean(b_values))
            h = int(mean(h_values))
            s = int(mean(s_values))
            v = int(mean(v_values))
            min_h = int(min(h_values))
            min_s = int(min(s_values))
            min_v = int(min(v_values))
            max_h = int(max(h_values))
            max_s = int(max(s_values))
            max_v = int(max(v_values))
            min_r = int(min(r_values))
            min_g = int(min(g_values))
            min_b = int(min(b_values))
            max_r = int(max(r_values))
            max_g = int(max(g_values))
            max_b = int(max(b_values))
            colors[c] = {
                "HSV_range_min":[min_h, min_s, min_v],
                "HSV_range_max":[max_h, max_s, max_v ],
                "HSV_mean":[h, s, v ],
                "RGB_range_min":[min_r, min_g, min_b],
                "RGB_range_max":[max_r, max_g, max_b],
                "RGB": [r,g,b]}
            print(f"{c}: {colors[c]}")
        rospack = rospkg.RosPack()
        path = rospack.get_path("vision_interface")
        jsonfile = f"{path}/database/colors.json"
        with open(jsonfile, "w") as outfile:
            json.dump(colors, outfile)


    def init_color(self):
        """Color Calibration Initialization"""
        self.current_color_id = 0
        self.current_color = list(self.color_values.keys())[self.current_color_id]
        print(f'Click to enter {self.current_color} value')
        print('Press q for the next color')

    def is_finished(self):
        """Check if color calibration is finished"""
        return self.current_color_id >= len(list(self.color_values.keys()))-1

    def next_color(self):
        """Go the the next color to calibrate"""
        self.current_color_id += 1
        self.current_color = list(self.color_values.keys())[self.current_color_id]
        print(f'Click to enter {self.current_color} value')
        print('Press q for the next color')

def main():
    rospy.init_node("color_calibration", anonymous=True)
    reader = Reader()
    rospy.loginfo("color_calibration Node Initialized")
    reader.init_color()
    r = rospy.Rate(30)
    while not rospy.is_shutdown():
        if(not reader.has_frame):
            r.sleep()
        else:
            cv2.imshow("Color Calibration",reader.frame)
            cv2.setMouseCallback('Color Calibration', reader.click_hsv)
            r.sleep()

            if(cv2.waitKey(33) & 0xFF in (ord('q'),27)):
                if(reader.is_finished()):
                    cv2.destroyAllWindows()
                    reader.export_values()
                    break
                else:
                    reader.next_color()
                    continue

if __name__ == "__main__":
    main()
