import cv2
import mediapipe as mp
import rospy
import rospkg
import yaml
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import copy
import numpy as np

from vision_driver.msg import PlatformMsg, Cell
from vision_interface.msg import Hand, HumanPose

def floor(x, min=0, max=1):
	if(x < min):
		return min
	elif(x > max):
		return max
	else:
		return x

class HandDetector :
	def __init__(
			self,
			mode=False,
			max_hands=2,
			model_complexity=1,
			detection_con=0.3,
			track_con = 0.3
		):
		"""Constructs hand detector"""
		rospack = rospkg.RosPack()
		path = rospack.get_path("vision_interface")
		yaml_file = f"{path}/config/sensors.yaml"
		self.topics = yaml.safe_load(open(yaml_file))

		self.mode = mode
		self.max_hands = max_hands
		self.model_complexity = model_complexity
		self.detection_con = detection_con
		self.track_con = track_con

		self.mp_hands = mp.solutions.hands
		self.hands = self.mp_hands.Hands(
			self.mode,
			self.max_hands,
			self.model_complexity,
			self.detection_con,
			self.track_con)
		self.mp_draw = mp.solutions.drawing_utils

		rospy.Subscriber(
			self.topics['rgb_image'],
			Image,
			self.callback_frame
		)

		rospy.Subscriber(
			self.topics['platform_position'],
			PlatformMsg,
			self.callback_platform
		)

		self.has_platform = False
		self.human_pose = HumanPose()

		self.has_frame = False
		self.cv2_image = None
		self.pub = rospy.Publisher(
			'/vision/vision_interface/human_pose',
			HumanPose,
			queue_size=1
		)

		rospy.loginfo('/vision/vision_interface/human_pose ready')

		self.publish(HumanPose())

	def publish(self, msg):
		self.pub.publish(msg)

	def callback_platform(self, platform):
		self.has_platform = True
		self.platform = platform

	def callback_frame(self, frame) :
		self.height = frame.height
		self.width = frame.width
		bridge = CvBridge()
		frame = bridge.imgmsg_to_cv2(frame)
		frame = np.array(cv2.cvtColor(
			frame,
			cv2.COLOR_BGR2RGB)
		)
		results = self.hands.process(frame)
		human_pose = HumanPose()

		if(not self.has_platform):
			print('No platform')
		elif(results.multi_hand_landmarks):
			print("HAND DETECTED")
			xmin = 1
			xmax = 0
			ymin = 1
			ymax = 0
			for hand in results.multi_hand_landmarks:
				print("******************************************************")
				print(hand)
				self.mp_draw.draw_landmarks(
					frame,
					hand,
					self.mp_hands.HAND_CONNECTIONS)
				for ld in hand.landmark:
					if ld.x<xmin :
						xmin = ld.x
					if ld.x>xmax :
						xmax = ld.x
					if ld.y<ymin :
						ymin = ld.y
					if ld.y>ymax :
						ymax = ld.y
				xmin = int(xmin*self.width)
				xmax = int(xmax*self.width)
				ymin = int(ymin*self.height)
				ymax = int(ymax*self.height)
				
				h = Hand()
				i_min = floor((xmin - self.platform.top_left.x) / (self.platform.top_right.x-self.platform.top_left.x))
				i_min = int(i_min * 44)
				j_min = floor((ymin - self.platform.top_left.y) / (self.platform.bottom_left.y-self.platform.top_left.y))
				j_min = int (j_min * 20)

				i_max = floor((xmax - self.platform.top_left.x) / (self.platform.top_right.x-self.platform.top_left.x))
				i_max = int(i_max * 44)
				j_max = floor((ymax - self.platform.top_right.y) / (self.platform.bottom_right.y-self.platform.top_right.y))
				j_max = int(j_max * 20)

				if(i_max > i_min+10):
					i_max = i_min+10
				if(j_max > j_min+10):
					j_max = j_min+10
				for i in range(i_min,i_max+1):
					for j in range(j_min, j_max+1):
						cell = Cell()
						cell.i = i
						cell.j = j
						h.occupied.append(cell)

				for j in range(j_min-1, 23):
					if(j<0 or j>22):
						continue
					if(j <= j_max):
						for i in range(i_min-1,i_max+2):
							if(i<0 or i>40):
								continue
							cell = Cell()
							cell.i = i
							cell.j = j
							h.possibly_occupied.append(cell)
					else:
						k = (j-j_max)+1
						for i in range(i_min-k,i_max+k+1):
							if(i<0 or i>40):
								continue
							cell = Cell()
							cell.i = i
							cell.j = j
							h.possibly_occupied.append(cell)


				# h.top_right.x = floor((xmax - self.platform.top_left.x) / (self.platform.top_right.x-self.platform.top_left.x))
				# h.top_right.y = floor((ymin - self.platform.top_right.y) / (self.platform.bottom_right.y-self.platform.top_right.y))
				#
				# h.bottom_left.x = floor((xmin - self.platform.top_left.x) / (self.platform.top_right.x-self.platform.top_left.x))
				# h.bottom_left.y = floor((ymax - self.platform.top_left.y) / (self.platform.bottom_left.y-self.platform.top_left.y))

				# h.bottom_right.x = floor((xmax - self.platform.top_left.x) / (self.platform.top_right.x-self.platform.top_left.x))
				# h.bottom_right.y = floor((ymax - self.platform.top_right.y) / (self.platform.bottom_right.y-self.platform.top_right.y))

				human_pose.hands.append(h)
		else:
			print("NO DETECTION")

		hand_detector.publish(human_pose)
		# print(frame)
		self.cv2_image = frame
		self.has_frame = True


rospy.init_node("hand_detector", anonymous=True)
hand_detector = HandDetector()
rospy.loginfo("hand_detector Node Initialized")
r = rospy.Rate(30)
while not rospy.is_shutdown():
	if(hand_detector.has_frame):
		# print(hand_detector.cv2_image)
		cv2.imshow("Hands Detection",hand_detector.cv2_image)
		r.sleep()
		if(cv2.waitKey(33) & 0xFF in (ord('q'),27)):
			cv2.destroyAllWindows()
			break
