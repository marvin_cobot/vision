from __future__ import print_function


""" A visualization tool for cell states.

This script subscribes to cell state messages, retrieves cell states with color information,
and visualizes the states on a graphical grid. The colors of each cell in the grid update
based on the received state information.
"""

__author__ = "Maxence Grand"
__credits__ = ["Maxence Grand, Loic Mazou"]
__license__ = "LGPL"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

WIDTH=1440
HEIGHT=720

import rospy
import json, yaml
import os, rospkg

from vision_interface.msg import CellState
from vision_driver.msg import Cell

import cv2
import math

global frame

def get_bgr(rgb):
	"""Convert RGB color to BGR format for OpenCV display.

    @param rgb: A list representing the RGB color.
    @type rgb: list

    @return: A list containing the color in BGR format with alpha channel.
    @rtype: list
    """
	c = []
	i = len(rgb)-1
	while i >= 0:
		c.append(rgb[i])
		i-=1
	c.append(255)
	return c

class Viewer:
	"""The cell state viewer, which displays cell states with color-coded visualization."""

	def __init__(self):
		"""Initialize the Viewer class.

        Loads necessary resources, including the background image and color database,
        and sets up ROS subscriptions to receive cell state information.
        """
		self.has_frame = False
		rospack = rospkg.RosPack()
		path = rospack.get_path("vision_interface")
		self.frame = cv2.imread(f"{path}/database/background.png")
		self.colors = json.load(open(f"{path}/database/colors.json"))
		self.table = yaml.safe_load(open(f"{path}/config/table.yaml"))
		self.h_cells = self.table['height']
		self.w_cells = self.table['width']
		rospy.loginfo("cell_state_visualization subscribes to the /vision/vision_interface/cell_state topic")
		rospy.Subscriber(
			"/vision/vision_interface/cell_state",
			CellState,
			self.show_state)

	def show_state(self, state):
		"""Initialize and run the cell state visualization node.

	    This function initializes the ROS node and Viewer instance, updates the display at a regular
	    interval, and terminates when 'q' or 'Esc' is pressed.
	    """
		#draw table

		for cell in state.cells:

			x = cell.i
			y = cell.j

			start = (
				int((x/self.w_cells) * WIDTH),
				int((y/self.h_cells) * HEIGHT)
			)

			end = (
				int(((x + 1)/self.w_cells) * WIDTH),
				int(((y + 1)/self.h_cells) * HEIGHT)
			)

			if(cell.color == 'undefined'):
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,100], -1)
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,255], 2)
				self.frame = cv2.line(self.frame, start, end, [0,0,255,255], 5)
			else:
				c = get_bgr(self.colors[cell.color]['RGB'])
				self.frame = cv2.rectangle(self.frame, start, end, c, -1)
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,255], 2)
		self.has_frame = True

def main():
	rospy.init_node("cell_state_visualization", anonymous=True)
	viewer = Viewer()
	rospy.loginfo('Node cell_state_visualization initialized')

	r = rospy.Rate(30)
	while not rospy.is_shutdown():
		if(not viewer.has_frame):
			r.sleep()
		else:
			cv2.imshow("Cells Viewer",viewer.frame)
			r.sleep()
			if(cv2.waitKey(33) & 0xFF in (ord('q'),27)):
				cv2.destroyAllWindows()
				break

if __name__ == "__main__":
	main()
