#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A RGB Camera Viewer Node."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"


import rospy

import json, rospkg, yaml, sys

import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError

from sensor_msgs.msg import Image
from vision_driver.msg import PlatformMsg

from pupil_apriltags import Detector

def centroid(points):
	"""Computes centroid"""
	x = [p[0] for p in points]
	y = [p[1] for p in points]
	centroid = (sum(x) / len(points), sum(y) / len(points))
	return centroid

class Viewer:

	def __init__(self):
		self.has_frame = False
		self.has_platform_position = False


		rospack = rospkg.RosPack()
		path = rospack.get_path("vision_interface")
		yaml_file = f"{path}/config/sensors.yaml"
		self.topics = yaml.safe_load(open(yaml_file))
		self.table = yaml.safe_load(open(f"{path}/config/table.yaml"))
		self.h_cells = self.table['height']
		self.w_cells = self.table['width']
		rospy.loginfo(f"RGB Viewer subscribes to the {self.topics['rgb_image']} topic")
		rospy.Subscriber(
			self.topics['rgb_image'],
			Image,
			self.callback_rgb
		)
		rospy.loginfo(f"RGB Viewer subscribes to the {self.topics['platform_position']} topic")
		rospy.Subscriber(
			self.topics['platform_position'],
			PlatformMsg,
			self.callback_platform
		)

	def draw_plateform(self, image):
		"""Draw the platform, i.e. draw each cell on thje platform"""
		if (self.has_platform_position):


			for cell in self.platform_position.cells:
				A = (int(cell.top_left.x),int(cell.top_left.y))
				B = (int(cell.top_right.x),int(cell.top_right.y))
				C = (int(cell.bottom_right.x),int(cell.bottom_right.y))
				D = (int(cell.bottom_left.x),int(cell.bottom_left.y))
				image = cv2.line(image, A, B, (0, 0, 255), 1)
				image = cv2.line(image, B, C, (0, 0, 255), 1)
				image = cv2.line(image, C, D, (0, 0, 255), 1)
				image = cv2.line(image, D, A, (0, 0, 255), 1)

		return image

	def callback_rgb(self, image):
		"""Receive a new image"""
		bridge = CvBridge()
		self.frame = self.draw_plateform(cv2.cvtColor(
			bridge.imgmsg_to_cv2(image),
			cv2.COLOR_BGR2RGB
		))
		self.has_frame = True

	def callback_platform(self, plat_pos):
		"""Check that the platform position is known"""
		self.platform_position = plat_pos
		self.has_platform_position = True

def main():
	rospy.init_node("rgb_image_viewer", anonymous=True)
	viewer = Viewer()
	rospy.loginfo("rgb_image_viewer Node Initialized")

	r = rospy.Rate(30)
	while not rospy.is_shutdown():
		if(not viewer.has_frame):
			r.sleep()
		else:
			cv2.imshow("RGB Image Viewer",viewer.frame)
			r.sleep()
			if(cv2.waitKey(33) & 0xFF in (ord('q'),27)):
				cv2.destroyAllWindows()
				break

if __name__ == "__main__":
	main()
