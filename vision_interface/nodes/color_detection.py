#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
The vision interface Node.

This node processes RGB images and identifies the dominant color in specific grid cells
on a platform. The detected color in each cell is published on the /vision/vision_interface/cell_state topic.
"""

__author__ = "Maxence Grand"
__credits__ = ["Maxence Grand, Loic Mazou"]
__license__ = "LGPL"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy

import json, rospkg, yaml, sys

import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError

from vision_driver.msg import PlatformMsg
from sensor_msgs.msg import Image
from vision_interface.msg import CellState
from vision_driver.msg import Cell
from collections import Counter
from math import floor, dist
import pandas as pd

def get_rgb(bgr):
	"""Convert BGR color format to RGB.

    Reverses the order of color values to convert BGR to RGB.

    @param bgr: List of BGR color values.
    @return: List of RGB color values.
    """
	c = []
	i = len(bgr)-1
	while i >= 0:
		c.append(bgr[i])
		i-=1
	return c

def get_dominant_color(inp_img):
	"""
	Function: get_dominant_color, to get the dominant color pixel values.
	https://stackoverflow.com/a/61730849

    param inp_img: Input image to analyze.
    @type inp_img: ndarray

    @return: Dominant color in HSV format.
    @rtype: list
	"""
	img = inp_img.copy()
	out = cv2.resize(img, (1, 1), 0)
	return out[0, 0]

def bgr_to_hsv(bgr):
	"""Convert a BGR color to HSV format.

    Calculates the HSV representation of the given BGR color value.

    @param bgr: List containing BGR color values.
    @return: List containing HSV color values.
    """
	b, g, r = bgr
	r, g, b = r/255.0, g/255.0, b/255.0
	mx = max(r, g, b)
	mn = min(r, g, b)
	df = mx-mn
	if mx == mn:
		h = 0
	elif mx == r:
		h = (60 * ((g-b)/df) + 360) % 360
	elif mx == g:
		h = (60 * ((b-r)/df) + 120) % 360
	elif mx == b:
		h = (60 * ((r-g)/df) + 240) % 360
	if mx == 0:
		s = 0
	else:
		s = (df/mx)*255 # (df/mx)*100
	v = mx*255 # mx*100
	h = h / 2
	return [floor(h), floor(s), floor(v)]

def dist_hsv(hsv1, hsv2) :
	"""Calculate a weighted distance between two HSV color values.

    This method calculates the distance in HSV color space, giving higher importance to the hue component.

    @param hsv1: List containing the first HSV color.
    @param hsv2: List containing the second HSV color.
    @return: Weighted distance as a float.
    """
	dh = min((hsv1[0]-hsv2[0])%180, (hsv2[0]-hsv1[0])%180)
	ds = abs(hsv1[1]-hsv2[1])
	dv = abs(hsv1[2]-hsv2[2])
	return((8*dh+4*ds+dv)/13) #ponderate mean in order to have high importance on H

def cmp_ranges(inp, low, high):
	"""Compare input HSV values with a range to check if they fall within the specified bounds.

    @param inp: List of HSV values to be compared.
    @param low: Lower bounds for the HSV values.
    @param high: Upper bounds for the HSV values.
    @return: Boolean indicating if HSV values are within range.
    """
	output = True
	for idx, elem in enumerate(inp):
		if (elem < low[idx] or elem > high[idx] ):
			output = False
	return output


class VisionInterface:
	"""Vision Interface Class to process images and identify colors in grid cells."""

	def __init__(self):
		"""Initialize the VisionInterface, load configurations, and set up ROS subscribers and publishers.

        Loads color data from JSON, sensor topics from YAML, and table configuration.
        Subscribes to RGB image and platform position topics, and initializes the cell state publisher.
        """
		rospack = rospkg.RosPack()
		path = rospack.get_path("vision_interface")
		csv_file = f"{path}/database/color_values.csv"
		df = pd.read_csv(csv_file)
		color_tabou = [
			'white',
			'black',
			'blue3',
			'brown3'
		]
		for c in color_tabou:
			mask = df['color_name'] == c
			df = df[~mask]
		self.color_dict = {
			color: group[['r', 'g', 'b']].values for color, group in df.groupby('color_name')
		}

		# self.color_dict = {}
		# for color, group in df.groupby('color_name'):
		# 	if(color in color_tabou):
		# 		continue
		# 	self.color_dict[color] = group[['r', 'g', 'b']].values
		self.color_means = df.groupby('color_name')[['r', 'g', 'b']].mean()
		# for c in color_tabou:
		# 	self.color_means.pop(c)
		yaml_file = f"{path}/config/sensors.yaml"
		self.topics = yaml.safe_load(open(yaml_file))
		yaml_file = f"{path}/config/topics.yaml"
		self.topics_to_pub = yaml.safe_load(open(yaml_file))
		self.table = yaml.safe_load(open(f"{path}/config/platform_position.yaml"))
		self.markers = yaml.safe_load(open(f"{path}/config/markers.yaml"))
		self.h_cells = self.table['height']
		self.w_cells = self.table['width']
		self.has_platform_position = False
		#subscribe to sensors topics
		rospy.loginfo(f"Vision Interface subscribes to the {self.topics['rgb_image']} topic")
		rospy.Subscriber(
			self.topics['rgb_image'],
			Image,
			self.callback_rgb
		)
		rospy.loginfo(f"Vision Interface subscribes to the {self.topics['platform_position']} topic")
		rospy.Subscriber(
			self.topics['platform_position'],
			PlatformMsg,
			self.callback_platform
		)

		self.pub_topics = {}
		self.init_cell_state()

	def find_two_closest_color_groups(self, rgb):
		"""Find the two closest color groups based on their mean values to the given RGB vector.

	    @param rgb: A 3-element list or array representing the RGB vector to compare.
	    @return: A tuple of the names of the two color groups with the closest mean RGB values.
	    """
		mean_values = self.color_means.values
		mean_names = self.color_means.index.values
		distances_squared = np.sum((mean_values - rgb) ** 2, axis=1)
		closest_indices = np.argsort(distances_squared)[:2]
		return mean_names[closest_indices]

	def mean_color_distance(self, rgb, c):
		"""Calculate the mean distance between the given RGB vector and all RGB values in a specific color group.

	    @param rgb: A 3-element list or array representing the RGB vector to compare.
	    @param c: The name of the color group to compare against.
	    @return: The average Euclidean distance between the input RGB vector and all RGB values in the specified color group.
	    """
		values = self.color_dict[c]
		distances = np.linalg.norm(values - rgb)
		return np.mean(distances)

	def find_closest_color(self, rgb, k=3):
		"""Find the closest color group to the input RGB vector by comparing with the nearest color groups.

	    @param rgb: A 3-element list or array representing the RGB vector to compare.
	    @param k: The number of nearest neighbors to consider when determining the closest color (default is 3).
	    @return: The name of the most frequent closest color group among the `k` nearest neighbors.
	    """
		c1,c2 = self.find_two_closest_color_groups(rgb)
		values = np.vstack((self.color_dict[c1], self.color_dict[c2]))
		color_names = np.array([c1] * len(self.color_dict[c1]) + [c2] * len(self.color_dict[c2]))
		distances_squared = np.sum((values - rgb) ** 2, axis=1)
		nearest_indices = np.argsort(distances_squared)[:k]
		nearest_colors = color_names[nearest_indices]
		most_common_color = Counter(nearest_colors).most_common(1)[0][0]
		return most_common_color

	def is_cell_marker(self, cell):
		"""Check if a cell is in a marker position"""
		for marker in self.markers.keys():
			pos = self.markers[marker]
			if(pos["top_left"]["x"] <= cell.i and \
				pos["bottom_right"]["x"] >= cell.i ):
				if(pos["top_left"]["y"] <= cell.j and\
					pos["bottom_right"]["y"] >= cell.j ):
					return True
		return False

	def init_cell_state(self):
		"""Initialize cell state * and publish the initial cell state.

        This creates a grid of undefined cells based on configuration and publishes them on initialization.
        """
		cells = []
		for i in range(1,self.h_cells+1):
			for j in range(1,self.h_cells+1):
				c = Cell()
				c.i = i
				c.j = j
				c.color = 'undefined'
				cells.append(c)
		cell_state = CellState()
		cell_state.cells = cells

		self.pub_topics['cell_state'] = rospy.Publisher(
			self.topics_to_pub['cell_state'],
			CellState,
			queue_size=1
		)

		rospy.loginfo("/vision/vision_interface/cell_state ready")
		self.pub_topics['cell_state'].publish(cell_state)

	def get_min_dist_hsv(self, hsv):
		"""Identify the closest predefined color to a given HSV value.

        Calculates the HSV distance between the input color and predefined colors, returning the closest match.

        @param hsv: HSV color values to be compared.
        @return: String representing the color with the minimum HSV distance.
        """
		min_c = 'green'
		min_dist = dist_hsv(hsv, self.colors[min_c]['HSV_mean'])
		for c in self.colors.keys():
			if cmp_ranges(
				hsv,
				self.colors[c]['HSV_range_min'],
				self.colors[c]['HSV_range_max']
			):
				d = dist_hsv(hsv, self.colors[c]['HSV_mean'])
				if(d < min_dist):
					min_dist = d
					min_c = c
		return min_c

	def get_min_dist_rgb(self, rgb):
		"""Identify the closest predefined color to a given RGB value.

        @param rgb: RGB color values to be compared.
        @return: String representing the color with the minimum RGB distance.
        """
		min_c = 'green'
		min_dist = dist(rgb, self.colors[min_c]['RGB'])
		for c in self.colors.keys():
			d = dist(rgb, self.colors[c]['RGB'])
			if(d < min_dist):
				min_dist = d
				min_c = c
		return min_c

	def update_cell_state(self, cv2_image):
		"""Update the color states of cells based on the current platform position and publish.

        Processes each cell, determining its dominant color, and then publishes the updated cell state.

        @param cv2_image: Image in OpenCV format for color analysis.
        """

		if(self.has_platform_position):

			cells = []
			start = rospy.get_time()*1000
			for c in self.platform_position.cells:
				if(self.is_cell_marker(c)):
					cells.append(c)
					continue
				A = (int(c.top_left.x),int(c.top_left.y))
				B = (int(c.top_right.x),int(c.top_right.y))
				C = (int(c.bottom_right.x),int(c.bottom_right.y))
				D = (int(c.bottom_left.x),int(c.bottom_left.y))
				cell = Cell()
				cell.i = c.i
				cell.j = c.j

				tmp = cv2_image[A[1]:C[1]]

				img_cell = np.array([tmp[i][A[0]:C[0]]
						for i in range(tmp.shape[0])] )
				bgr = get_dominant_color(img_cell)
				hsv = bgr_to_hsv(bgr)
				rgb = get_rgb(bgr)
				cell.color=self.find_closest_color(rgb)
				if(cell.color == 'shadow'):
					cell.color = 'green'
				cells.append(cell)

			cell_state = CellState()
			cell_state.cells = cells
			self.pub_topics['cell_state'].publish(cell_state)
			end = rospy.get_time()*1000
			print(end - start)
		else:
			rospy.loginfo("Platform position is unknown")

	def callback_rgb(self, image):
		"""Handle new RGB image messages.

        Converts the ROS image message to OpenCV format and calls update_cell_state.

        @param image: The received ROS Image message.
        """
		bridge = CvBridge()
		cv2_image = cv2.cvtColor(
			bridge.imgmsg_to_cv2(image),
			cv2.COLOR_BGR2RGB
		)
		self.update_cell_state(cv2_image)

	def callback_platform(self, plat_pos):
		"""Handle platform position messages.

        Updates the platform position and flags that the position data is available.

        @param plat_pos: PlatformMsg message containing cell positions.
        """
		self.platform_position = plat_pos
		self.has_platform_position = True

def main():
	rospy.init_node("vision_interface", anonymous=True)
	vision_interface = VisionInterface()
	rospy.loginfo("vision_interface Node Initialized")
	rospy.spin()

if __name__ == "__main__":
	main()
