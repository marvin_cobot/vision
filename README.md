# Vision


>This repo is a fork of this [repo](https://gricad-gitlab.univ-grenoble-alpes.fr/pacbot_group/state_controller)

>This module is part of the [Human-ABB Yumi Collaboration Project](https://gitlab.com/marvin_cobot/controller). For more details of all the modules required to use this module, please refer to this project.

## Overview


![Texte alternatif](docs/vision.png "Vision Module Architecture")

The **vision** package is a ROS-based system designed for image-based platform and object detection, integrating functionalities from its sub-packages: `vision_driver` and `vision_interface`. This package facilitates the detection of platform boundaries using AprilTags, the identification of objects within a predefined platform grid, and the tracking of hands and colors within that grid. By processing image data and generating cell and position information, it provides a foundation for high-level robotics applications.

## Sub-packages

### Vision Driver

The **vision_driver** package focuses on detecting the platform area and objects on it by locating AprilTags positioned at the platform corners. The node computes coordinates for these tags, creates a grid overlay on the platform, and publishes positional information to ROS topics for use in other nodes.

For more detailed installation and configuration instructions, refer to the `vision_driver` subdirectory.

### Vision Interface

The **vision_interface** package handles color and hand detection within the platform’s grid structure. Using the platform grid layout and coordinates from `vision_driver`, this sub-package identifies colors in each cell and tracks hand positions over the platform. The data is then published as ROS messages for other nodes to process.

For more detailed installation and configuration instructions, refer to the `vision_interface` subdirectory.

## Installation

### Prerequisites

This package requires:

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS Noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)
- [OpenCV](https://pypi.org/project/opencv-python/)
- [cv_bridge](http://wiki.ros.org/cv_bridge)
- [AprilTag](https://github.com/AprilRobotics)

### Installing Dependencies

Install dependencies as required by both sub-packages:

```bash
sudo apt-get install ros-noetic-cv-bridge
pip install opencv-python
pip install pupil-apriltags
```

### Building the Package

To clone the vision package, including all its sub-packages, into your ROS workspace, use:
```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/marvin_cobot/vision.git
cd ~/catkin_ws
catkin build
source devel/setup.bash
```

## Usage

To run the **vision** package, first launch the nodes for both sub-packages:
```bash
roslaunch vision vision.launch
```

This setup will initialize platform detection, color calibration, and hand tracking functionalities, making ROS topics available for use in high-level applications.

## Licence

This project is licensed under the LGPL License

## Maintainers

- [Maxence Grand](Maxence.Grand@univ-grenoble-alpes.fr), Research Engineer, Laboratoire D'informatique de Grenoble, Univ. Grenoble Alpes, Grenoble 38000 France
